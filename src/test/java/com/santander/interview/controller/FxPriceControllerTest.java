package com.santander.interview.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.santander.interview.domain.InstrumentName;
import com.santander.interview.model.FxPriceDto;
import com.santander.interview.repository.FxPriceRepository;
import com.santander.interview.service.FxPriceCommissionService;
import com.santander.interview.service.FxPriceService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static com.santander.interview.FxPriceModelCreator.getFxPriceDto;
import static com.santander.interview.FxPriceModelCreator.getFxPricesDto;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class FxPriceControllerTest {

    @Autowired
    MockMvc mockMvc;
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    FxPriceService fxPriceService;
    @Autowired
    FxPriceRepository fxPriceRepository;
    @Autowired
    FxPriceCommissionService fxPriceCommissionService;

    @BeforeEach
    void setup() {
        fxPriceRepository.deleteAll();
    }

    @Test
    void shouldGetFxPrices() throws Exception {

        fxPriceService.addFxPrices(getFxPricesDto());

        assertEquals(3, fxPriceRepository.findAll().size());

        this.mockMvc.perform(get("/v1/api/fx-prices" + "/{instrumentName}", InstrumentName.EUR_USD)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.externalId", equalTo(106)))
                .andExpect(jsonPath("$.instrumentName", equalTo(InstrumentName.EUR_USD.name())))
                .andExpect(jsonPath("$.bid").exists())
                .andExpect(jsonPath("$.ask").exists())
                .andExpect(jsonPath("$.timestamp").exists());

        this.mockMvc.perform(get("/v1/api/fx-prices" + "/{instrumentName}", InstrumentName.EUR_JPY)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.externalId", equalTo(107)))
                .andExpect(jsonPath("$.instrumentName", equalTo(InstrumentName.EUR_JPY.name())))
                .andExpect(jsonPath("$.bid").exists())
                .andExpect(jsonPath("$.ask").exists())
                .andExpect(jsonPath("$.timestamp").exists());

        this.mockMvc.perform(get("/v1/api/fx-prices" + "/{instrumentName}", InstrumentName.GBP_USD)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.externalId", equalTo(108)))
                .andExpect(jsonPath("$.instrumentName", equalTo(InstrumentName.GBP_USD.name())))
                .andExpect(jsonPath("$.bid").exists())
                .andExpect(jsonPath("$.ask").exists())
                .andExpect(jsonPath("$.timestamp").exists());
    }

    @Test
    void shouldGetNewerFxPrice() throws Exception {

        Long firstId = 10L;
        Long secondId = 20L;
        Long thirdId = 30L;

        FxPriceDto firstFxPriceDto = getFxPriceDto(firstId);
        FxPriceDto secondFxPriceDto = getFxPriceDto(secondId);
        FxPriceDto thirdFxPriceDto = getFxPriceDto(thirdId);

        fxPriceService.addFxPrices(List.of(firstFxPriceDto, secondFxPriceDto, thirdFxPriceDto));

        assertEquals(3, fxPriceRepository.findAll().size());

        this.mockMvc.perform(get("/v1/api/fx-prices" + "/{instrumentName}", firstFxPriceDto.getInstrumentName())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.externalId").value(thirdId));
    }

    @Test
    void shouldReturnNotFoundWhenNoFxPriceInCacheAndDb() throws Exception {

        assertEquals(0, fxPriceRepository.findAll().size());

        this.mockMvc.perform(get("/v1/api/fx-prices" + "/{instrumentName}", InstrumentName.EUR_USD)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}
