package com.santander.interview.consumer;

import com.santander.interview.model.FxPriceDto;
import com.santander.interview.service.FxPriceCsvService;
import com.santander.interview.service.FxPriceService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class FxPriceFeedConsumerListenerTest {

    @InjectMocks
    FxPriceFeedConsumerListener fxPriceFeedConsumerListener;
    @Mock
    FxPriceCsvService fxPriceCsvService;
    @Mock
    FxPriceService fxPriceService;

    @Test
    void shouldExecuteOnMessage() {

        String message = "message";
        List<FxPriceDto> fxPricesDto = List.of();

        when(fxPriceCsvService.getFxPricesFromCsvMessage(message)).thenReturn(fxPricesDto);

        fxPriceFeedConsumerListener.onMessage(message);

        verify(fxPriceCsvService, times(1)).getFxPricesFromCsvMessage(message);
        verify(fxPriceService, times(1)).addFxPrices(fxPricesDto);
    }
}