package com.santander.interview.service;

import com.santander.interview.domain.FxPrice;
import com.santander.interview.model.FxPriceDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.santander.interview.FxPriceModelCreator.getFxPrice;
import static com.santander.interview.FxPriceModelCreator.getFxPriceDto;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class FxPriceMapperTest {

    @InjectMocks
    FxPriceMapper fxPriceMapper;

    @Test
    void shouldMapYoFxPrice() {

        FxPriceDto fxPriceDto = getFxPriceDto();
        FxPrice fxPrice = fxPriceMapper.toFxPrice(fxPriceDto);

        assertEquals(fxPriceDto.getExternalId(), fxPrice.getExternalId());
        assertEquals(fxPriceDto.getInstrumentName(), fxPrice.getInstrumentName());
        assertEquals(fxPriceDto.getBid(), fxPrice.getBid());
        assertEquals(fxPriceDto.getAsk(), fxPrice.getAsk());
        assertEquals(fxPriceDto.getTimestamp(), fxPrice.getTimestamp());
    }

    @Test
    void shouldMapYoFxPriceDto() {

        FxPrice fxPrice = getFxPrice();
        FxPriceDto fxPriceDto = fxPriceMapper.toFxPriceDto(fxPrice);

        assertEquals(fxPrice.getExternalId(), fxPriceDto.getExternalId());
        assertEquals(fxPrice.getInstrumentName(), fxPriceDto.getInstrumentName());
        assertEquals(fxPrice.getBid(), fxPriceDto.getBid());
        assertEquals(fxPrice.getAsk(), fxPriceDto.getAsk());
        assertEquals(fxPrice.getTimestamp(), fxPriceDto.getTimestamp());
    }
}
