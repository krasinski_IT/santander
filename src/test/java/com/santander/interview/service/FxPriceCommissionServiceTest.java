package com.santander.interview.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class FxPriceCommissionServiceTest {

    @InjectMocks
    FxPriceCommissionService fxPriceCommissionService;

    @Test
    void shouldCalculateBidWithCommission() {

        BigDecimal bid = BigDecimal.TEN;
        BigDecimal bidWithCommission = fxPriceCommissionService.calculateBidWithCommission(bid);

        assertEquals(new BigDecimal("9.990"), bidWithCommission);
    }

    @Test
    void shouldCalculateAskWithCommission() {

        BigDecimal ask = BigDecimal.TEN;
        BigDecimal asdWithCommission = fxPriceCommissionService.calculateAskWithCommission(ask);

        assertEquals(new BigDecimal("10.010"), asdWithCommission);
    }
}
