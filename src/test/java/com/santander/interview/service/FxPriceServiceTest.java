package com.santander.interview.service;

import com.santander.interview.domain.FxPrice;
import com.santander.interview.domain.InstrumentName;
import com.santander.interview.exceptionhandler.FxPriceNotFoundException;
import com.santander.interview.model.FxPriceDto;
import com.santander.interview.repository.FxPriceRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static com.santander.interview.FxPriceModelCreator.getFxPrice;
import static com.santander.interview.FxPriceModelCreator.getFxPricesDto;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class FxPriceServiceTest {

    @InjectMocks
    FxPriceService fxPriceService;
    @Mock
    FxPriceRepository fxPriceRepository;
    @Mock
    FxPriceMapper fxPriceMapper;
    @Mock
    FxPriceCommissionService fxPriceCommissionService;

    @Test
    void shouldGetFxPrice() {

        InstrumentName instrumentName = InstrumentName.EUR_USD;
        FxPrice fxPriceInRepository = getFxPrice();

        when(fxPriceRepository.findFirstByInstrumentNameOrderByExternalIdDesc(instrumentName)).thenReturn(Optional.of(fxPriceInRepository));

        FxPrice fxPrice = fxPriceService.getFxPrice(instrumentName);

        verify(fxPriceRepository, times(1)).findFirstByInstrumentNameOrderByExternalIdDesc(instrumentName);

        assertEquals(fxPriceInRepository.getExternalId(), fxPrice.getExternalId());
        assertEquals(fxPriceInRepository.getInstrumentName(), fxPrice.getInstrumentName());
        assertEquals(fxPriceInRepository.getBid(), fxPrice.getBid());
        assertEquals(fxPriceInRepository.getAsk(), fxPrice.getAsk());
        assertEquals(fxPriceInRepository.getTimestamp(), fxPrice.getTimestamp());
    }

    @Test
    void shouldThrowFxPriceNotFoundExceptionWhenNoFxPriceInRepository() {

        InstrumentName instrumentName = InstrumentName.EUR_USD;

        when(fxPriceRepository.findFirstByInstrumentNameOrderByExternalIdDesc(instrumentName)).thenReturn(Optional.empty());

        Assertions.assertThrows(FxPriceNotFoundException.class, () -> {
            fxPriceService.getFxPrice(instrumentName);
        });
    }

    @Test
    void shouldGetFxPriceFromCache() {

        InstrumentName instrumentName = InstrumentName.EUR_USD;
        FxPrice fxPriceInRepository = getFxPrice();

        when(fxPriceRepository.findFirstByInstrumentNameOrderByExternalIdDesc(instrumentName)).thenReturn(Optional.of(fxPriceInRepository));

        fxPriceService.getFxPrice(instrumentName);

        verify(fxPriceRepository, times(1)).findFirstByInstrumentNameOrderByExternalIdDesc(instrumentName);

        fxPriceService.getFxPrice(instrumentName);
        verify(fxPriceRepository, times(1)).findFirstByInstrumentNameOrderByExternalIdDesc(instrumentName);
    }

    @Test
    void shouldAddFxPrices() {

        List<FxPriceDto> fxPricesDto = getFxPricesDto();
        int size = fxPricesDto.size();

        when(fxPriceCommissionService.calculateBidWithCommission(any())).thenReturn(BigDecimal.TEN);
        when(fxPriceCommissionService.calculateAskWithCommission(any())).thenReturn(BigDecimal.TEN);
        when(fxPriceMapper.toFxPrice(any())).thenReturn(getFxPrice());

        fxPriceService.addFxPrices(fxPricesDto);

        verify(fxPriceMapper, times(size)).toFxPrice(any());
        verify(fxPriceCommissionService, times(size)).calculateBidWithCommission(any());
        verify(fxPriceCommissionService, times(size)).calculateAskWithCommission(any());
        verify(fxPriceRepository, times(size)).save(any());
    }
}
