package com.santander.interview.service;

import com.santander.interview.domain.InstrumentName;
import com.santander.interview.exceptionhandler.FxPriceValidationException;
import com.santander.interview.model.FxPriceDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

import static com.santander.interview.service.FxPriceCsvService.FORMATTER;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class FxPriceCsvServiceTest {

    @InjectMocks
    FxPriceCsvService fxPriceCsvService;

    @Test
    void shouldGetFxPriceDtoWithOneLineCsvMessage() {

        String message = "106, EUR/USD, 1.1000,1.2000,01-06-2020 12:01:01:001";

        List<FxPriceDto> fxPricesDto = fxPriceCsvService.getFxPricesFromCsvMessage(message);

        assertEquals(1, fxPricesDto.size());

        FxPriceDto fxPriceDto = fxPricesDto.get(0);

        assertEquals(106L, fxPriceDto.getExternalId());
        assertEquals(InstrumentName.valueOfName("EUR/USD"), fxPriceDto.getInstrumentName());
        assertEquals(new BigDecimal("1.1000"), fxPriceDto.getBid());
        assertEquals(new BigDecimal("1.2000"), fxPriceDto.getAsk());
        assertEquals(Instant.from(FORMATTER.parse("01-06-2020 12:01:01:001")), fxPriceDto.getTimestamp());
    }

    @Test
    void shouldGetFxPricesDtoWithTwoLinesCsvMessage() {

        String message = "106, EUR/USD, 1.1000,1.2000,01-06-2020 12:01:01:001" + "\n" +
                "107, EUR/JPY, 119.60,119.90,01-06-2020 12:01:02:002";

        List<FxPriceDto> fxPricesDto = fxPriceCsvService.getFxPricesFromCsvMessage(message);

        assertEquals(2, fxPricesDto.size());
    }

    @Test
    void shouldThrownExceptionWhenEmptyMessage() {

        Assertions.assertThrows(FxPriceValidationException.class, () -> {
            fxPriceCsvService.getFxPricesFromCsvMessage("");
        });

        Assertions.assertThrows(FxPriceValidationException.class, () -> {
            fxPriceCsvService.getFxPricesFromCsvMessage(null);
        });
    }

    @Test
    void shouldThrownExceptionWhenInvalidExternalId() {

        Assertions.assertThrows(FxPriceValidationException.class, () -> {
            fxPriceCsvService.getFxPricesFromCsvMessage("106a, EUR/USD, 1.1000,1.2000,01-06-2020 12:01:01:001");
        });
    }

    @Test
    void shouldThrownExceptionWhenInvalidInstrumentName() {

        Assertions.assertThrows(FxPriceValidationException.class, () -> {
            fxPriceCsvService.getFxPricesFromCsvMessage("106, EUR/USDD, 1.1000,1.2000,01-06-2020 12:01:01:001");
        });
    }

    @Test
    void shouldThrownExceptionWhenInvalidBid() {

        Assertions.assertThrows(FxPriceValidationException.class, () -> {
            fxPriceCsvService.getFxPricesFromCsvMessage("106, EUR/USD, asd1.1000,1.2000,01-06-2020 12:01:01:001");
        });
    }

    @Test
    void shouldThrownExceptionWhenInvalidAsk() {

        Assertions.assertThrows(FxPriceValidationException.class, () -> {
            fxPriceCsvService.getFxPricesFromCsvMessage("106, EUR/USD, 1.1000,1.2y000,01-06-2020 12:01:01:001");
        });
    }

    @Test
    void shouldThrownExceptionWhenInvalidTimestamp() {

        Assertions.assertThrows(FxPriceValidationException.class, () -> {
            fxPriceCsvService.getFxPricesFromCsvMessage("106, EUR/USD, 1.1000,1.2000,01-06-20-20 12:01:01:001");
        });
    }
}
