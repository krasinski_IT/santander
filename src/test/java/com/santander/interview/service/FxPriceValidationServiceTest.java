package com.santander.interview.service;

import com.santander.interview.domain.FxPrice;
import com.santander.interview.exceptionhandler.FxPriceValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;

@ExtendWith(MockitoExtension.class)
class FxPriceValidationServiceTest {

    @InjectMocks
    FxPriceValidationService fxPriceValidationService;

    @Test
    void shouldThrowExceptionWhenBidGreaterThanAsk() {

        FxPrice fxPrice = new FxPrice();
        fxPrice.setAsk(BigDecimal.TEN);
        fxPrice.setBid(fxPrice.getAsk().add(BigDecimal.ONE));

        Assertions.assertThrows(FxPriceValidationException.class, () -> {
            fxPriceValidationService.validateFxPrice(fxPrice);
        });
    }

    @Test
    void shouldThrowExceptionWhenBidEqualToAsk() {

        FxPrice fxPrice = new FxPrice();
        fxPrice.setAsk(BigDecimal.TEN);
        fxPrice.setBid(fxPrice.getAsk());

        Assertions.assertThrows(FxPriceValidationException.class, () -> {
            fxPriceValidationService.validateFxPrice(fxPrice);
        });
    }

    @Test
    void shouldDoNothingWhenBigLowerThanAsk() {

        FxPrice fxPrice = new FxPrice();
        fxPrice.setAsk(BigDecimal.TEN);
        fxPrice.setBid(fxPrice.getAsk().subtract(BigDecimal.ONE));

        fxPriceValidationService.validateFxPrice(fxPrice);
    }
}
