package com.santander.interview;

import com.santander.interview.domain.FxPrice;
import com.santander.interview.domain.InstrumentName;
import com.santander.interview.model.FxPriceDto;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

import static com.santander.interview.service.FxPriceCsvService.FORMATTER;

public class FxPriceModelCreator {

    public static List<FxPriceDto> getFxPricesDto() {

        return List.of(
                FxPriceDto.builder()
                        .externalId(106L)
                        .instrumentName(InstrumentName.EUR_USD)
                        .bid(new BigDecimal("1.1000"))
                        .ask(new BigDecimal("1.2000"))
                        .timestamp(Instant.from(FORMATTER.parse("01-06-2020 12:01:01:001")))
                        .build(),
                FxPriceDto.builder()
                        .externalId(107L)
                        .instrumentName(InstrumentName.EUR_JPY)
                        .bid(new BigDecimal("119.60"))
                        .ask(new BigDecimal("119.90"))
                        .timestamp(Instant.from(FORMATTER.parse("01-06-2020 12:01:02:002")))
                        .build(),
                FxPriceDto.builder()
                        .externalId(108L)
                        .instrumentName(InstrumentName.GBP_USD)
                        .bid(new BigDecimal("1.2500"))
                        .ask(new BigDecimal("1.2560"))
                        .timestamp(Instant.from(FORMATTER.parse("01-06-2020 12:01:02:002")))
                        .build()
        );
    }

    public static FxPrice getFxPrice() {

        return FxPrice.builder()
                .externalId(106L)
                .instrumentName(InstrumentName.EUR_USD)
                .bid(new BigDecimal("1.1000"))
                .ask(new BigDecimal("1.2000"))
                .timestamp(Instant.from(FORMATTER.parse("01-06-2020 12:01:01:001")))
                .build();
    }

    public static FxPriceDto getFxPriceDto() {

        return FxPriceDto.builder()
                .externalId(106L)
                .instrumentName(InstrumentName.EUR_USD)
                .bid(new BigDecimal("1.1000"))
                .ask(new BigDecimal("1.2000"))
                .timestamp(Instant.from(FORMATTER.parse("01-06-2020 12:01:01:001")))
                .build();
    }

    public static FxPriceDto getFxPriceDto(Long externalId) {

        return FxPriceDto.builder()
                .externalId(externalId)
                .instrumentName(InstrumentName.EUR_USD)
                .bid(new BigDecimal("1.1000"))
                .ask(new BigDecimal("1.2000"))
                .timestamp(Instant.from(FORMATTER.parse("01-06-2020 12:01:01:001")))
                .build();
    }
}
