package com.santander.interview.service;

import com.santander.interview.domain.FxPrice;
import com.santander.interview.exceptionhandler.FxPriceValidationException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FxPriceValidationService {

    public void validateFxPrice(FxPrice fxPrice) {

        if (fxPrice.getBid().compareTo(fxPrice.getAsk()) >= 0) {
            throw new FxPriceValidationException("ask has to be greater than bid");
        }
    }
}
