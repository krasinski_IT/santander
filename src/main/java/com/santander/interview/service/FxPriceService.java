package com.santander.interview.service;

import com.santander.interview.domain.FxPrice;
import com.santander.interview.domain.InstrumentName;
import com.santander.interview.exceptionhandler.FxPriceNotFoundException;
import com.santander.interview.model.FxPriceDto;
import com.santander.interview.repository.FxPriceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Service
@RequiredArgsConstructor
public class FxPriceService {

    private final ConcurrentHashMap<InstrumentName, FxPrice> fxPriceCache = new ConcurrentHashMap<>(3);
    private final FxPriceRepository fxPriceRepository;
    private final FxPriceMapper fxPriceMapper;
    private final FxPriceCommissionService fxPriceCommissionService;

    public FxPrice getFxPrice(InstrumentName instrumentName) {

        if (fxPriceCache.containsKey(instrumentName)) {
            return fxPriceCache.get(instrumentName);
        }

        FxPrice fxPrice = fxPriceRepository.findFirstByInstrumentNameOrderByExternalIdDesc(instrumentName)
                .orElseThrow(() -> new FxPriceNotFoundException(instrumentName));

        fxPriceCache.put(instrumentName, fxPrice);

        return fxPrice;
    }

    public void addFxPrices(List<FxPriceDto> fxPricesDto) {

        for (FxPriceDto fxPriceDto : fxPricesDto) {

            FxPrice fxPrice = fxPriceMapper.toFxPrice(fxPriceDto);
            fxPrice.setBid(fxPriceCommissionService.calculateBidWithCommission(fxPrice.getBid()));
            fxPrice.setAsk(fxPriceCommissionService.calculateAskWithCommission(fxPrice.getAsk()));

            fxPriceRepository.save(fxPrice);
            fxPriceCache.put(fxPriceDto.getInstrumentName(), fxPrice);
        }
    }
}
