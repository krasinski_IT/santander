package com.santander.interview.service;

import com.santander.interview.domain.FxPrice;
import com.santander.interview.model.FxPriceDto;
import org.springframework.stereotype.Service;

@Service
public class FxPriceMapper {

    public FxPrice toFxPrice(FxPriceDto fxPriceDto) {
        return FxPrice.builder()
                .externalId(fxPriceDto.getExternalId())
                .instrumentName(fxPriceDto.getInstrumentName())
                .bid(fxPriceDto.getBid())
                .ask(fxPriceDto.getAsk())
                .timestamp(fxPriceDto.getTimestamp())
                .build();
    }

    public FxPriceDto toFxPriceDto(FxPrice fxPrice) {
        return FxPriceDto.builder()
                .externalId(fxPrice.getExternalId())
                .instrumentName(fxPrice.getInstrumentName())
                .bid(fxPrice.getBid())
                .ask(fxPrice.getAsk())
                .timestamp(fxPrice.getTimestamp())
                .build();
    }
}
