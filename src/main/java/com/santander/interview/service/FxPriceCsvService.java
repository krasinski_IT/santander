package com.santander.interview.service;

import com.santander.interview.domain.InstrumentName;
import com.santander.interview.exceptionhandler.FxPriceValidationException;
import com.santander.interview.model.FxPriceDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FxPriceCsvService {

    public static final DateTimeFormatter FORMATTER = DateTimeFormatter
            .ofPattern("dd-MM-yyyy HH:mm:ss:SSS")
            .withZone(ZoneId.systemDefault());

    public List<FxPriceDto> getFxPricesFromCsvMessage(String message) {

        List<FxPriceDto> fxPricesDto = new ArrayList<>();

        try {
            String[] lines = message.split("\\r?\\n");

            for (String line : lines) {

                String[] elements = line.split(",");

                Long externalId = Long.valueOf(elements[0].trim());
                InstrumentName instrumentName = InstrumentName.valueOfName(elements[1].trim());
                BigDecimal bid = new BigDecimal(elements[2].trim());
                BigDecimal ask = new BigDecimal(elements[3].trim());
                Instant instant = Instant.from(FORMATTER.parse(elements[4].trim()));

                fxPricesDto.add(FxPriceDto.builder()
                        .externalId(externalId)
                        .instrumentName(instrumentName)
                        .bid(bid)
                        .ask(ask)
                        .timestamp(instant)
                        .build());
            }
            return fxPricesDto;
        } catch (Exception e) {
            throw new FxPriceValidationException("csv message is not valid");
        }
    }
}
