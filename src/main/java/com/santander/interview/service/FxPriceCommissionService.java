package com.santander.interview.service;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class FxPriceCommissionService {

    private static final BigDecimal BID_COMMISSION = new BigDecimal("0.001");
    private static final BigDecimal ASK_COMMISSION = new BigDecimal("0.001");

    public BigDecimal calculateBidWithCommission(BigDecimal bid) {
        return bid.subtract(bid.multiply(BID_COMMISSION));
    }

    public BigDecimal calculateAskWithCommission(BigDecimal ask) {
        return ask.add(ask.multiply(ASK_COMMISSION));
    }
}
