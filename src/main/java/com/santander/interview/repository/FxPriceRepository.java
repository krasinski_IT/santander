package com.santander.interview.repository;

import com.santander.interview.domain.FxPrice;
import com.santander.interview.domain.InstrumentName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface FxPriceRepository extends JpaRepository<FxPrice, Long> {

    Optional<FxPrice> findFirstByInstrumentNameOrderByExternalIdDesc(InstrumentName instrumentName);
}
