package com.santander.interview.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ErrorMsg {
    private Integer code;
    private String error;
}
