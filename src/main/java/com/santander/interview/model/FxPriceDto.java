package com.santander.interview.model;

import com.santander.interview.domain.InstrumentName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.Instant;

@Getter
@Setter
@Builder
public class FxPriceDto {

    private Long externalId;
    private InstrumentName instrumentName;
    private BigDecimal bid;
    private BigDecimal ask;
    private Instant timestamp;
}
