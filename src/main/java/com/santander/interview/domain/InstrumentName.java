package com.santander.interview.domain;

import com.santander.interview.exceptionhandler.FxPriceValidationException;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@AllArgsConstructor
@Getter
public enum InstrumentName {

    EUR_USD("EUR/USD"),
    GBP_USD("GBP/USD"),
    EUR_JPY("EUR/JPY");

    private final String name;

    public static InstrumentName valueOfName(String name) {

        return Arrays.stream(values())
                .filter(instrumentName -> instrumentName.name.equals(name))
                .findFirst()
                .orElseThrow(() -> new FxPriceValidationException("Not supported InstrumentName: " + name));
    }
}
