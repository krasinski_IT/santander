package com.santander.interview.consumer;

public interface FxPriceFeedConsumer {

    void onMessage(String message);
}
