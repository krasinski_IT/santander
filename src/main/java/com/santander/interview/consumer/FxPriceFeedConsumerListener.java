package com.santander.interview.consumer;

import com.santander.interview.model.FxPriceDto;
import com.santander.interview.service.FxPriceCsvService;
import com.santander.interview.service.FxPriceService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FxPriceFeedConsumerListener implements FxPriceFeedConsumer {

    private final FxPriceCsvService fxPriceCsvService;
    private final FxPriceService fxPriceService;

    @Override
    public void onMessage(String message) {
        List<FxPriceDto> fxPricesFromCsvMessage = fxPriceCsvService.getFxPricesFromCsvMessage(message);
        fxPriceService.addFxPrices(fxPricesFromCsvMessage);
    }
}
