package com.santander.interview.exceptionhandler;

import com.santander.interview.domain.InstrumentName;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;

public class FxPriceNotFoundException extends ResponseStatusException {

    private static final String ERROR_MESSAGE = "Fx price not found, instrumentName: ";

    public FxPriceNotFoundException(InstrumentName instrumentName) {
        super(HttpStatus.NOT_FOUND, ERROR_MESSAGE + instrumentName);
    }
}
