package com.santander.interview.exceptionhandler;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class FxPriceValidationException extends ResponseStatusException {

    private static final String ERROR_MESSAGE = "Fx price validation exception: ";

    public FxPriceValidationException(String message) {
        super(HttpStatus.BAD_REQUEST, ERROR_MESSAGE + message);
    }
}
