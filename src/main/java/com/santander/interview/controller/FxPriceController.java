package com.santander.interview.controller;

import com.santander.interview.domain.InstrumentName;
import com.santander.interview.model.ErrorMsg;
import com.santander.interview.model.FxPriceDto;
import com.santander.interview.service.FxPriceMapper;
import com.santander.interview.service.FxPriceService;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/api/fx-prices")
@RequiredArgsConstructor
@Api(tags = {"Fx prices"})
@SwaggerDefinition
public class FxPriceController {

    private final FxPriceService fxPriceService;
    private final FxPriceMapper fxPriceMapper;

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful read"),
            @ApiResponse(code = 400, message = "Bad request", response = ErrorMsg.class),
            @ApiResponse(code = 404, message = "Not found", response = ErrorMsg.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorMsg.class)
    })
    @ApiOperation(value = "Get FX price by instrument name")
    @GetMapping("/{instrumentName}")
    public FxPriceDto getFxPrice(@PathVariable InstrumentName instrumentName) {
        return fxPriceMapper.toFxPriceDto(fxPriceService.getFxPrice(instrumentName));
    }
}
