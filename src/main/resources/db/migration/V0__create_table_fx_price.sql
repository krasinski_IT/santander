CREATE TABLE FX_PRICE
(
    id              BIGINT IDENTITY (1,1),
    external_id     BIGINT,
    instrument_name VARCHAR(7),
    bid             DECIMAL(10, 4),
    ask             DECIMAL(10, 4),
    timestamp       DATETIME,
    CONSTRAINT PK_FX_PRICE_ID PRIMARY KEY (id)
);

CREATE INDEX IX_FX_PRICE_IN_ID on FX_PRICE (instrument_name, external_id);
