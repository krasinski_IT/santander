# FX price feed app

- [Swagger](http://localhost:8080/swagger-ui.html)

## Requirements

- JDK 11
- Maven 3

## Launching the app

```
mvn spring-boot:run
```

## Running test

```
mvn test
```
